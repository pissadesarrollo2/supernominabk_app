﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupSupernominaDB
{
    class Backup
    {
        public void backupHESTIA() { }
        public void backupITCO() { }
        public void backupMULTISERV() { }
        public void backupSERVINPROF() { }
        public void backupTALHUM() { }

        public void runBackup(SqlConnection conn,string dbname) {
        
            SqlCommand cmd = new SqlCommand("sp_SupernominaBackup", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            try 
            {
                Console.WriteLine("Abriendo conexión a la base de datos "+ dbname +"...");
                conn.Open();
                Console.WriteLine("Conexión establecida...");
                Console.WriteLine("Ejecutando el backup " + dbname);
                cmd.ExecuteNonQuery();
                Console.WriteLine("Backup realizado con éxito");
                Console.WriteLine();
                //Console.WriteLine("Presione cualquier tecla para salir...");
                //Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error al crear el back up: " + e.Message);
                //Console.WriteLine("Presione cualquier tecla para salir...");
                //Console.ReadKey();
            }
            finally
            {
                conn.Close();
            }      

        }


    }
}
