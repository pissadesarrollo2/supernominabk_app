﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace BackupSupernominaDB
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Iniciando el back up de las bases de datos de supernomina...");
            Console.WriteLine();
            Backup bk = new Backup();

            bk.runBackup(new SqlConnection(ConfigurationManager.ConnectionStrings["prodHESTIADB"].ToString()), "HESTIA");
            bk.runBackup(new SqlConnection(ConfigurationManager.ConnectionStrings["prodITCODB"].ToString()), "ITCO");
            bk.runBackup(new SqlConnection(ConfigurationManager.ConnectionStrings["prodMULTISERVDB"].ToString()), "MULTISERV");
            bk.runBackup(new SqlConnection(ConfigurationManager.ConnectionStrings["prodSERVINPROFDB"].ToString()), "SERVINPROF");
            bk.runBackup(new SqlConnection(ConfigurationManager.ConnectionStrings["prodTALHUMDB"].ToString()), "TALHUM");

            Environment.Exit(0);
        }
    }
}
